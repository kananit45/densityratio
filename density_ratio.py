# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import itertools
from sklearn.model_selection import KFold


class RelativeDensityRatioEstimator(object):
    u"""相対密度比推定."""

    def __init__(self, nf, nbasis=1000, beta=0.5, ls=1., lam=0.01, random_state=0):
        u"""
        初期化.

        :引数:
        nf : 入力データの次元数
        nbasis : 射影する空間の次元数. 3乗オーダーで計算量が増加するので注意
        """
        self.beta = beta            # 相対重要度パラメータ
        self.ls = ls                # ガウスカーネルのスケール
        self.lam = lam              # 正則化パラメータ(l2)
        self.alpha = None           # 学習した重み
        self.nbasis = nbasis
        np.random.seed(random_state)
        self.w = np.random.normal(size=(nf, nbasis))
        self.b = np.random.rand(nbasis) * 2 * np.pi
    
    def kernel(self, x):
        u"""
        非線形空間に射影する.

        基底はガウスカーネルをrandom feature mapで近似したもの.
        """
        return np.sqrt(2) * np.cos(
            np.dot(x / self.ls, self.w) + self.b[np.newaxis, :]) / np.sqrt(self.nbasis)

    def get_GH(self, qx, px):
        phi_q = self.kernel(qx)
        phi_p = self.kernel(px)
        G = self.beta * np.dot(phi_q.T, phi_q) / qx.shape[0]\
            + (1 - self.beta) * np.dot(phi_p.T, phi_p) / px.shape[0]
        h = np.mean(phi_q, axis=0)
        return G, h
        
    def fit(self, qx, px):
        u"""密度比の推定."""
        G, h = self.get_GH(qx, px)
        self.alpha = np.linalg.solve(G + self.lam * np.eye(self.nbasis), h)
    
    def predict(self, x):
        u"""密度比の予測."""
        phi = self.kernel(x)
        return np.dot(phi, self.alpha)
    
    def score(self, qx, px):
        u"""新データに対する損失の計算."""
        G, h = self.get_GH(qx, px)
        return .5 * np.dot(self.alpha, np.dot(G, self.alpha)) - np.dot(self.alpha, h)


def cv_scoring(px, qx, model, n_splits=3, random_state=None):
    u"""CVスコアを返す."""
    cv_helper = KFold(n_splits=n_splits, random_state=random_state)

    scores = []
    for (train_p, test_p), (train_q, test_q) in zip(cv_helper.split(px), cv_helper.split(qx)):
        model.fit(px[train_p, :], qx[train_q, :])
        scores.append(model.score(px[test_p, :], qx[test_q, :]))
    return np.mean(scores)


def grid_search_cv(requires, params, px, qx, n_splits=3, random_state=None):
    u"""
    グリッドサーチを実行する.

    **引数**
    requires : 固定の引数(dict)
    params : チューニングしたいパラメータ名とその候補のリスト(dict)
    px, qx : 訓練データ
    """
    combinations = itertools.product(*(params[key] for key in params.keys()))
    score_table = pd.DataFrame(list(combinations), columns=params.keys())

    print("{ncomb} combinations with {cv}Fold Cross Validation".format(
        ncomb=score_table.shape[0], cv=n_splits))

    scores = []
    for i in score_table.index:
        par = dict(score_table.iloc[i])
        kwg = {**requires, **par}
        score = cv_scoring(px, qx, RelativeDensityRatioEstimator(**kwg),
            n_splits=n_splits, random_state=random_state)
        scores.append(score)
    score_table["_score"] = scores
    score_table = score_table.sort_values("_score")
    return score_table
